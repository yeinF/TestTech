﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechRover;
using TechTestData;

namespace TechTestGUI
{
    public class LauncherRover
    {
        private RoverBussines rb = new RoverBussines();
        private string position;

        public void LaunchRover()
        {
            ShowPositionRover pr = new ShowPositionRover();
            string initial = rb.PositionInitial(); // get initial position and show  
            position = initial;
            pr.showInitial(initial);

            while (true)
            {            
               // Console.WriteLine("Instruction for Rover (L,R,F): ");
                pr.message("Instruction for Rover (L,R,F): ");
                var command = Console.ReadLine().ToString().ToUpper();  // configuration for var type String and Upper Always             
                string result;
                if (!validCommand(command))
                {
                    result = processRover(command.ToString());
                    if (!rb.gridPosition(result))
                    {
                        pr.messageError(result);
                        rb.changePosition(position);  // save in the class position
                        result = position;            // show in the var position
                    }
                    else
                        rb.changePosition(result);
                    pr.showRover(result);

                }
                else
                    pr.message("invalid command");                                       
            }
        }

        public string processRover(string command)
        {
            return rb.processPosition(command);
        }

        public Boolean validCommand(string command)
        {
            // Validation always instruction is Upper            
            return (command != "L" && command != "R" && command != "F");
         
        }
       
    }
}
